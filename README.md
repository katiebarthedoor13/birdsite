# BirdSite

A browser extension for cross-posting Mastodon toots to Twitter.

## Features

- Cross-post a toot to Twitter right from the Mastodon web app.
- Attached images are also cross-posted.
- Messages longer than a tweet are truncated, and a link to the original toot is added.

![Screenshot of BirdSite UI: an 'Also post on the bird site' checkbox is added to Mastodon's compose form.](assets/chrome-web-store-banner.png)

## Usage

1. Install the extension for [Firefox](https://addons.mozilla.org/fr/firefox/addon/birdsite/) or [Chrome](https://chrome.google.com/webstore/detail/birdsite/lfmfhaopllgidldpmifdcjdckdggdjaj);
2. Open the web page of your favorite Mastodon instance;
3. Type your toot;
4. Click the “Also send to the bird site” button on the toot compose form;
5. Toot!

If you are not authenticated on Twitter yet, a pop-up will open to authenticate you,
and to ask for permission to post the tweet in your name. You only have to do this once.

Afterwards, toots sent while the checkbox is ticked will also be posted on your Twitter account.

## Alternatives

- Prefer cross-posting from Twitter (or TweetDeck) to Mastodon? _Have a look at the great [Tooter](https://github.com/ineffyble/tooter/) browser extension._
- Rather have all your toots and tweets cross-posted automatically? _Try the awesome [Mastodon Twitter Poster](https://crossposter.masto.donte.com.br/), and setup automatic mirroring between your microblogging accounts._
- Using native mobile apps? _The [Twidere](https://github.com/TwidereProject/Twidere-Android) app can cross-post to both Mastodon and Twitter._

## How to build

1. Clone the repository;
2. `make all`

## How does it work

**Retrieving the toot content**: Mastodon web UI doesn't have the hooks to give an extension access to the full toot details. If the toot only contains plain-text, this is fine: we don't need more informations, so the toot content is immediately retrieved from the web UI textarea.

But when there the toot has attached medias, it gets trickier: we need the full toot metadata. This is also the case if the toot is too long to fit in a tweet: we need to retrieve the toot public URL to append it to the tweet.

In this case, we need a way to get the content of the toot programmatically – if possible without asking for additional API permissions. To perform this, when the toot is posted, BirdSite polls the user public RSS feed, and does some fuzzy-matching to find the toot that was just posted. Using the metadatas in the RSS item, we can access attached medias, get the public toot URL, and construct a tweet to be posted.

**Posting the tweet**: To post tweets on a user behalf, we need to implement an OAuth flow for signing in to Twitter. OAuth requests need to be signed digitally, and are generally painfull to implement in a browser context. Fortunately, browsers implement a [identity-request mechanism](https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/API/identity/launchWebAuthFlow) that perform the OAuth flow for us. Unfortunately, this works only for OAuth2 – and Twitter user API has been stuck in OAuth 1.0a for ages.

So BirdSite implement an entire OAuth client, complete with SHA1 parameters signing and all. Access tokens to the Twitter API are saved in the browser extension localStorage, to be re-used if present. If the authentication fails (for instance because the tokens expired), we just go through the auth flow once more.

Once we get through the auth flow, posting a tweet isn't that hard. First we upload all media attachements using the chunked upload API (more complex, but it allows us to post larger attachements like GIFs). Then we create the tweet from the toot text, truncate it if needed, add the references to the uploaded attachements, and post it through the Twitter API.

## License

This extension is licensed under the terms of the GPL v3 license. (see LICENSE.md)
